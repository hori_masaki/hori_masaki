<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理</title>
    </head>
    <body>
        <div class="header">

		        <a href="./home.jsp">ホーム</a>
		        <a href="settings">ユーザー編集</a>
		        <a href="signup">ユーザー登録</a>
		</div>
		<div class="users">
    <c:forEach items="${users}" var="getUsers">
            <div class="getUsers">
                <div class="getUsers">
                    <span class="login_id"><c:out value="${getUsers.login_id}" /></span>
                    <span class="name"><c:out value="${getUsers.name}" /></span>
                    <span class="shiten_id"><c:out value="${getUsers.shiten_id}" /></span>
					<input type="button" onClick="" value="復活">
                    <input type="button" onClick="" value="停止">
                    <a href="<%= response.encodeURL("settings.jsp") %>">ユーザー編集</a>
                </div>
            </div>
    </c:forEach>
</div>

            <div class="copyright"> Copyright(c)HoriMasaki</div>
    </body>
</html>
