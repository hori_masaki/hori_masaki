<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>
<body>
		        <a href="toukou">新規投稿画面</a>
                <a href="./">ユーザー管理</a>
                <a href="logout">ログアウト</a>

				<c:if test="${ not empty loginUser }">
				    <div class="profile">
				        <div class="login_id"><h2><c:out value="${loginUser.login_id}" /></h2></div>
				        <div class="name">
				            @<c:out value="${loginUser.name}" />
				        </div>
				        <div class="shiten_id">
				            <c:out value="${loginUser.shiten_id}" />
				        </div>
				    </div>
				</c:if>
                            <div class="copyright"> Copyright(c)HoriMasaki</div>
</body>
</html>