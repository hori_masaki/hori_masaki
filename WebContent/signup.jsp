<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br /> <label for="login_id">ログインID</label> <input name="login_id" id="login_id" /> <br />

                <label for="password">パスワード</label> <input name="password"
                    id="password" /> <br />
                <label for="k_password">確認パスワード</label> <input name="k_password"
                    id="k_password" /> <br />

                     <label for="name">名前</label> <input
                    name="name"  id="name" /> <br />
                    <label for="shiten_id">支店</label> <input name="shiten_id" id="shiten_id" /> <br />
                    <label for="class_id">部署・役職</label> <input name="class_id" id="class_id" /> <br />

                <br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)HoriMasaki</div>
        </div>
    </body>
</html>