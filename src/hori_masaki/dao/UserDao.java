package hori_masaki.dao;

import static hori_masaki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import hori_masaki.beans.User;
import hori_masaki.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append(" login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", shiten_id");
            sql.append(", class_id");
            sql.append(", suspension");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // shiten_id
            sql.append(", ?"); // class_id
            sql.append(", ?"); // suspension
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getShiten_id());
            ps.setInt(5, user.getClass_id());
            ps.setInt(6, user.getSuspension());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


public User getUser(Connection connection, String login_id,
        String password) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE login_id= ? AND password = ?";

        ps = connection.prepareStatement(sql);
        ps.setString(1, login_id);
        ps.setString(2, password);

        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

private List<User> toUserList(ResultSet rs) throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
            int id = rs.getInt("id");
            String login_id = rs.getString("login_id");
            String password = rs.getString("password");
            String name = rs.getString("name");
            int shiten_id = rs.getInt("shiten_id");
            int class_id = rs.getInt("class_id");
            int suspension = rs.getInt("suspension");
            Timestamp createdDate = rs.getTimestamp("created_date");
            Timestamp updatedDate = rs.getTimestamp("updated_date");

            User user = new User();
            user.setId(id);
            user.setLogin_id(login_id);
            user.setPassword(password);
            user.setName(name);
            user.setShiten_id(shiten_id);
            user.setClass_id(class_id);
            user.setSuspension(suspension);
            user.setCreatedDate(createdDate);
            user.setUpdatedDate(updatedDate);

            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
}
public User getUser(Connection connection, int id) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE id = ?";

        ps = connection.prepareStatement(sql);
        ps.setInt(1, id);

        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}
public List<User> getUsers(Connection connection) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users ";

        ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
            return userList;

    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}
}