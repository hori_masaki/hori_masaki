package hori_masaki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hori_masaki.beans.User;
import hori_masaki.service.UserService;


@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setShiten_id(Integer.parseInt(request.getParameter("shiten_id")));
            user.setClass_id(Integer.parseInt(request.getParameter("class_id")));
            user.setSuspension(1);

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String shiten_id = request.getParameter("shiten_id");
        String class_id = request.getParameter("class_id");
        String k_password = request.getParameter("k_password");
        int moji = name.length();
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログイン名を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(shiten_id) == true) {
            messages.add("支店コードを入力してください");
        }
        if (StringUtils.isEmpty(class_id) == true) {
            messages.add("部署・役職コードを入力してください");
        }
        if (!login_id.matches("[a-zA-Z0-9]{6,20}")) {
        	messages.add("ログインIDを正しく入力してください");
        }
        if (!password.matches("[!-~]{6,20}")) {
        	messages.add("パスワードを正しく入力してください");
        }
        if (! k_password.equals(password)) {
        	messages.add("確認パスワードが正しくありません");
        }
        if (!shiten_id.matches("^[0-9]*$")){
        	messages.add("数字で入力してください");
        }
        if (!class_id.matches("^[0-9]*$")){
        	messages.add("数字で入力してください");
        }
        if ( moji > 10){
        	messages.add("１０文字以下で入力してください");
        }


        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}