package hori_masaki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hori_masaki.beans.User;
import hori_masaki.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        //セッションよりログインユーザーの情報を取得
        User loginUser = (User) session.getAttribute("loginUser");
        //ログインユーザー情報のidを元にDBからユーザー情報取得
        User editUser = new UserService().getUser(loginUser.getId());
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }
}