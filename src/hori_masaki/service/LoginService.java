package hori_masaki.service;

import static hori_masaki.utils.CloseableUtil.*;
import static hori_masaki.utils.DBUtil.*;

import java.sql.Connection;

import hori_masaki.beans.User;
import hori_masaki.dao.UserDao;
import hori_masaki.utils.CipherUtil;

public class LoginService {

    public User login(String login_id, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, login_id, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}